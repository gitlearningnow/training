package com.qa.clinet;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chiranjeevi on 05/05/2020.
 */
public class RestClient {

    //GET Method
    public CloseableHttpResponse getMethod(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        return httpClient.execute(httpGet);
    }

    public CloseableHttpResponse getMethodWithHeaders(String url, HashMap<String,String> map) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        for(Map.Entry<String,String> entry : map.entrySet()){
            httpGet.addHeader(entry.getKey(), entry.getValue());
        }

        return httpClient.execute(httpGet);
    }
}
