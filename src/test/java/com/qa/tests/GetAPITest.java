package com.qa.tests;

import com.qa.base.TestBase;
import com.qa.clinet.RestClient;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Chiranjeevi on 06/05/2020.
 */
public class GetAPITest extends TestBase {
    TestBase testBase;
    String url;
    RestClient restClient;
    CloseableHttpResponse httpResponse;

    @BeforeMethod
    public void setUp(){
        testBase = new TestBase();
        url = prop.getProperty("URL")+prop.getProperty("serviceURL");
    }

    @Test(priority = 1)
    public void getApiTestWithOutHeaders() throws IOException {
        restClient = new RestClient();
        httpResponse = restClient.getMethod(url);

        //Status code
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(statusCode,RESPONSE_STATUS_CODE_200, "Status code doesn't match" );
        System.out.println("Status Code is ====== "+statusCode);

        //get json body
        String responseString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        JSONObject jsonResponse = new JSONObject(responseString);
        System.out.println("jsonResponse is ====== "+jsonResponse);
        // All heasders
        Header[]  headerArrya = httpResponse.getAllHeaders();
        HashMap allHeaders = new HashMap<String ,String>();
        for(Header header : headerArrya){
            allHeaders.put(header.getName(), header.getValue());
        }
        System.out.println("allHeaders is ====== "+allHeaders);
    }

    @Test(priority = 2)
    public void getApiTestWithHeaders() throws IOException {
        HashMap<String, String> headersMap = new HashMap<String, String>();
        headersMap.put("ContentType", "application/json");
        restClient = new RestClient();
        httpResponse = restClient.getMethodWithHeaders(url, headersMap);

        //Status code
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        Assert.assertEquals(statusCode,RESPONSE_STATUS_CODE_200, "Status code doesn't match" );
        System.out.println("Status Code is ====== "+statusCode);

        //get json body
        String responseString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
        JSONObject jsonResponse = new JSONObject(responseString);
        System.out.println("jsonResponse is ====== "+jsonResponse);
        // All heasders
        Header[]  headerArrya = httpResponse.getAllHeaders();
        HashMap allHeaders = new HashMap<String ,String>();
        for(Header header : headerArrya){
            allHeaders.put(header.getName(), header.getValue());
        }
        System.out.println("allHeaders is ====== "+allHeaders);
    }
    }
